package ru.kulakov.servletTest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
//� ���� ������� web.xml ����� �������� �� ��������� @WebServlet("/")
public class ServletTest extends HttpServlet {
	@Override
	  public void doGet(HttpServletRequest request,
	                    HttpServletResponse response)
	      throws ServletException, IOException {
	    response.setContentType("text/html"); //������ ������ ������ - HTML, �����
	    PrintWriter out = response.getWriter(); //�������� ������, ����������� �������� ������� � �����
	 
	    out.write("<!DOCTYPE html>\n" + // ���������� � ����� HTML ��� ���������� ���������
	       "<html>\n" +
	       "<head><title>Simple servlet application.</title></head>\n" +
	       "<body>\n" +
	       "<br><br> The called method is: " +
	       request.getMethod() +
	       "\n</body></html>"); 
	  }
}
