<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Test</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://dl.dropboxusercontent.com/u/18821118/JTimers.js"></script>
 <script type="text/javascript">
 var questionsAsked = 0;
 function getAnswers(answers){
	 var ansArr = new Array();
	 ansArr[0] = "";
	 var j = 0;
	 for(var i = 0; i< answers.length; i++){
	 	if(answers.charAt(i)==';'){
	 		j++;
	 		ansArr[j] = "";
	 	}
	 else
	 	ansArr[j]+=answers.charAt(i);
	 }
	 return ansArr;
}
 
 function firstQuestion(){
	 questionsAsked = 0;
		$.ajax({
			  url : 'getFirstQuestion',
		 	  type: 'GET',
		 	  dataType: 'json',
			  contentType: 'application/json',
			  mimeType: 'application/json',
			  success: function (data) { 
			    
			   var answers = getAnswers(data.answers);
			   $('div.question').html("Вопрос 1:<br>"+data.question+"<span style=\"padding:0px 120px;\"><h4 id=\"timer\" style=\"display:inline;\"></h4></span>");
			   for(var i=0; i<answers.length; i++){
				   $('form.answers').append("<input type=\"radio\" name=\"answer\" value = \""+i+"\"/> "+answers[i]+"<br>");
			   }
			   $('button.submitAnswer').text("Ответить");
			   $('button.submitAnswer').attr('onclick', 'nextQuestion()');
			   $("#timer").everyTime(1000, function(i) {
					 $(this).text(30-i);
					 if(i==30){
						 var selected = $("input:radio:checked");
						 	if(selected[0]==null){ //Если нет помечных екоксов помечаем первый
						 		$('input:radio').each( function() {
						 	        if($(this).val()==0)
						 	          this.checked = !this.checked;
						 	      });
						 	}
						 	$('button.submitAnswer').click();
					 }
					});	 
			  }
			 });
		
		
		
 }
 function nextQuestion(){
	 var selected = $("input:radio:checked");
	 if(questionsAsked==4){
		 $.ajax({
			  url : 'getResult',
		 	  type: 'GET',
		 	  dataType: 'json',
			  contentType: 'application/json',
			  mimeType: 'application/json',
			  data : ({
					   	res: selected[0].value
				 	  }),
			  success: function (data) { 
				  $('div.question').html("Вы ответили верно на "+ data +" из 5 вопросов.   <a href =\"logout\">Выйти</a> <br><br><a href =\"stats\">Статистика</a>");
				  $('form.answers').html("");
				  $('button.submitAnswer').text("Начать заново");
				  $('button.submitAnswer').attr('onclick', 'firstQuestion()');
			  }
			 });
	 }
	 else{
		 $.ajax({
			  url : 'nextQuestion',
		 	  type: 'GET',
		 	  dataType: 'json',
			  contentType: 'application/json',
			  mimeType: 'application/json',
			  data : ({
					   	res: selected[0].value
				 	  }),
			  success: function (data) { 
			    
			   var answers = getAnswers(data.answers);
			   $('div.question').html("Вопрос "+ (questionsAsked+1) +":<br>"+data.question +"<span style=\"padding:0px 120px;\"><h4 id=\"timer\" style=\"display:inline;\"></h4></span>" + "<br>");
			   $('form.answers').html("");
			   for(var i=0; i<answers.length; i++){
				   $('form.answers').append("<input type=\"radio\" name=\"answer\" value = \""+i+"\"/> "+answers[i]+"<br>");
			   }
			   $("#timer").everyTime(1000, function(i) {
					 $(this).text(30-i);
					 if(i==30){
						 var selected = $("input:radio:checked");
						 	if(selected[0]==null){ //Если нет помечных екоксов помечаем первый
						 		$('input:radio').each( function() {
						 	        if($(this).val()==0)
						 	          this.checked = !this.checked;
						 	      });
						 	}
						 	$('button.submitAnswer').click();
					 }
					});	 
			  }
			 });
		 
		 questionsAsked++;
	 }
 }
 </script>

</head>
<body>
<div id='page'>
    <div class="question">Система тестирования знаний</div>
	<br>
	<br>
	<form class="answers"></form>
	<br>
    <button onclick="firstQuestion()" class="submitAnswer">Пройти тест</button>
</div>
</body>
</html>