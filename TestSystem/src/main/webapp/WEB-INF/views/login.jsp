<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Sign Up</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	 <script type="text/javascript">
	 function swithToSignUp(){
		 var res = '<h2>Sign Up</h2><br><form:form method="POST" action="addUser" commandName="user"><table><tr><td><form:label path="username">Логин</form:label></td><td><form:input path="username" /></td></tr>';
			res+='<tr><td><form:label path="password">Пароль</form:label></td><td><form:input path="password" type = "password"  /></td></tr>';
			res+='<tr><td colspan="2" align="right"><input type="submit"value="Sign Up" /></td></tr>';
			res+='<tr><td><br><span class="link-span" style ="color:blue; text-decoration:underline; cursor:pointer;" onclick="swithToSignIn();">SignIn</span></tr></table></form:form>';
		$("form.SignForm").html(res);
	 	$("form.SignForm").attr('action', 'addUser'); 
	 }
	function swithToSignIn(){
		 var res = "<h2>Sign In</h2><br><table><tr><td align=\"right\">Логин</td><td><input type=\"text\" name=\"j_username\" /></td></tr>";
		 res+="<tr><td align=\"right\">Пароль</td><td><input type=\"password\" name=\"j_password\" /></td></tr>";
		 res+="<tr><td align=\"right\">Запомнить меня</td><td><input type=\"checkbox\" name=\"_spring_security_remember_me\" /></td></tr>";
		 res+="<tr><td colspan=\"2\" align=\"right\"><input type=\"submit\" value=\"Sign In\" /><input type=\"reset\" value=\"Reset\" /></td></tr>";
		 res+="<tr><td><span class=\"link-span\" style =\"color:blue; text-decoration:underline; cursor:pointer;\" onclick=\"swithToSignUp();\">SignUp</span></tr></table>";
	 	$("form.SignForm").html(res);
	 	$("form.SignForm").attr('action', '/j_spring_security_check'); 
	 }
	 </script>
</head>
<body>


<c:if test="${not empty param.error}">
	<font color="red"> "Ошибка входа"
	: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </font>
</c:if>
<form class ="SignForm" method="POST" action="<c:url value="/j_spring_security_check" />">
<h2>Sign In</h2><br>
<table>
	<tr>
		<td align="right">Логин</td>
		<td><input type="text" name="j_username" /></td>
	</tr>
	<tr>
		<td align="right">Пароль</td>
		<td><input type="password" name="j_password" /></td>
	</tr>
	<tr>
		<td align="right">Запомнить меня</td>
		<td><input type="checkbox" name="_spring_security_remember_me" /></td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="submit" value="Sign In" />
		<input type="reset" value="Reset" /></td>
	</tr>
	<tr>
	<td><span class="link-span" style ="color:blue; text-decoration:underline; cursor:pointer;" onclick="swithToSignUp();">SignUp</span>
	</tr>
</table>
</form>
</body>
</html>