<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Статистика</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://dygraphs.com/1.0.1/dygraph-combined.js"></script>
<script type="text/javascript">
var results = [];
var myResults = [];
var myResultDate = [];
<c:forEach var="res" items="${results}">
	results.push("${res.result}");
	if("${res.username}"=="${myName}"){
		myResults.push("${res.result}");
		myResultDate.push("${res.date}")
	}
</c:forEach>

function updatePage(){
	$('div.yourResult').append((sum(myResults)/myResults.length).toFixed(2));
	$('div.totalResult').append((sum(results)/results.length).toFixed(2));
	
	var res = " ";
	for(var i=0;i<myResults.length;i++){ //сборщик данных для диаграмы
 		var str = new String(myResultDate[i]);
		res+= str.substring(0,16) +","+myResults[i]+"\n"; 
	}
	g = new Dygraph(

		    // containing div
		    document.getElementById("graphdiv"),

		    // CSV or path to a CSV file.
		    res

		  );
}


function sum(array){
	var sum = new Number;
	for(var i=0;i<array.length;i++)
		sum+=Number(array[i]);
	return sum;
}



</script>
</head>
<body>
<div class="yourResult">Ваш средний результат:  </div>
<div class="totalResult">Общий средний результат:  </div>
<br>
<h2>Ваши результаты:</h2>
<br>
<div id="graphdiv"></div>
<script type="text/javascript">
updatePage();
</script>
<br>
<a href="test">Назад</a>
</body>
</html>