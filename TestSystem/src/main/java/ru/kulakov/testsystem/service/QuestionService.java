package ru.kulakov.testsystem.service;

import java.util.List;

import ru.kulakov.testsystem.domain.Question;

public interface QuestionService {
	public List<Question> listQuestion();
}
