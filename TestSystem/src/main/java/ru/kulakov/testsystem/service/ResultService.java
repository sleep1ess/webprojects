package ru.kulakov.testsystem.service;

import java.util.List;

import ru.kulakov.testsystem.domain.Result;

public interface ResultService {
	public List<Result> listResult();
	public void addResult(Result result);
}
