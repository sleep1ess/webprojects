package ru.kulakov.testsystem.service;

import java.util.List;

import ru.kulakov.testsystem.domain.User;

public interface UserService {
	public List<User> listUser();
	public void addUser(User user);
}
