package ru.kulakov.testsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.kulakov.testsystem.dao.QuestionDAO;
import ru.kulakov.testsystem.domain.Question;

@Service
public class QuestionServiceImpl implements QuestionService {

	 @Autowired
	 private QuestionDAO questionDAO;

	
	@Transactional
	public List<Question> listQuestion() {
		
		return questionDAO.listQuestion();
	}

}
