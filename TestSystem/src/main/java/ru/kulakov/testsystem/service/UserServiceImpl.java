package ru.kulakov.testsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.kulakov.testsystem.dao.UserDAO;
import ru.kulakov.testsystem.domain.User;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;
	
	@Transactional
	public List<User> listUser() {
		return userDAO.listUser();
	}

	@Transactional
	public void addUser(User user) {
		userDAO.addUser(user);
	}

}
