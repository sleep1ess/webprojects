package ru.kulakov.testsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.kulakov.testsystem.dao.ResultDAO;
import ru.kulakov.testsystem.domain.Result;

@Service
public class ResultServiceImpl implements ResultService {

	@Autowired
	private ResultDAO resultDAO;
	
	@Transactional
	public List<Result> listResult() {
		return resultDAO.listResult();
	}

	@Transactional
	public void addResult(Result result) {
		resultDAO.addResult(result);
	}

}
