package ru.kulakov.testsystem.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.kulakov.testsystem.domain.Question;


@Repository
public class QuestionDAOImpl implements QuestionDAO {
	
	@Autowired 
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<Question> listQuestion() {
		Session session = sessionFactory.getCurrentSession();
	    List<Question> questions = session.createQuery("from Question").list();
	    return questions;
	}

}
