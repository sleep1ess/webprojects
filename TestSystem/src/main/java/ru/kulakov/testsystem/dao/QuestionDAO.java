package ru.kulakov.testsystem.dao;

import java.util.List;

import ru.kulakov.testsystem.domain.Question;

public interface QuestionDAO {
	 public List<Question> listQuestion();
}
