package ru.kulakov.testsystem.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.kulakov.testsystem.domain.Result;

@Repository
public class ResultDAOImpl implements ResultDAO {
	
	@Autowired 
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<Result> listResult() {
		Session session = sessionFactory.getCurrentSession();
		List<Result> result = session.createQuery("from Result").list();
	    return result;
	}

	public void addResult(Result result) {
		sessionFactory.getCurrentSession().save(result);
	}

}
