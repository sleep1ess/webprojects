package ru.kulakov.testsystem.dao;

import java.util.List;

import ru.kulakov.testsystem.domain.User;

public interface UserDAO {
	 public List<User> listUser();
	 public void addUser(User user);
}
