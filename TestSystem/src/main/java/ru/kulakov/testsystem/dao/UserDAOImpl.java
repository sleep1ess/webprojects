package ru.kulakov.testsystem.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.kulakov.testsystem.domain.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired 
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<User> listUser() {
		Session session = sessionFactory.getCurrentSession();
		List<User> users = session.createQuery("from User").list();
	    return users;
	}

	public void addUser(User user) {
		sessionFactory.getCurrentSession().save(user);


	}

}
