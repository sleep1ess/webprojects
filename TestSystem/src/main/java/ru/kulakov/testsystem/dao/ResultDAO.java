package ru.kulakov.testsystem.dao;

import java.util.List;

import ru.kulakov.testsystem.domain.Result;

public interface ResultDAO {
	 public List<Result> listResult();
	 public void addResult(Result result);
}
