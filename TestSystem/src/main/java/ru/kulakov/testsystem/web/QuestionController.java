package ru.kulakov.testsystem.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.kulakov.testsystem.domain.Question;
import ru.kulakov.testsystem.domain.Result;
import ru.kulakov.testsystem.service.QuestionService;
import ru.kulakov.testsystem.service.ResultService;

@Controller
@RequestMapping("/")
public class QuestionController {
   
	@Autowired
    private QuestionService questionService;
	@Autowired
    private ResultService resultService;
	
	private ArrayList<Question> questions;
	private int[] questionNumbers;
	private int correctAnswers;
	private int currentCorrectAnswer;// � ���� ��������� �� ������ ���������� ����� ���� �� ���������� ��� ������������
	private int questionsAsked;
	
	
	@RequestMapping("/test")
	public String test(Model model) {
		return "test";
	}
	@RequestMapping(value = "/getFirstQuestion", method = RequestMethod.GET)
	public @ResponseBody Question getFirstQuestion() { 
		correctAnswers = 0;
		questionsAsked = 0;
		questionNumbers = new int[5];
		questions = (ArrayList<Question>) questionService.listQuestion();
		for(int i=0 ; i<5;){
			int num = (int) (Math.random()*questions.size());
			if(!contains(questionNumbers, num)){
				questionNumbers[i]=num;
				i++;
			}
			  
		}
		currentCorrectAnswer = questions.get(questionNumbers[0]).getAnswer();
		questions.get(questionNumbers[0]).setAnswer(-1);
  	  	return questions.get(questionNumbers[0]);
  	 }
 
	
	@RequestMapping(value = "/nextQuestion", method = RequestMethod.GET)
	public @ResponseBody Question nextQuestion(@RequestParam String res) {
		if(questionsAsked==4)
			return questions.get(questionNumbers[questionsAsked]);
		else{
			int answer = Integer.parseInt(res)+1;
			if(currentCorrectAnswer==answer)
				correctAnswers++;
			questionsAsked++;
			currentCorrectAnswer = questions.get(questionNumbers[questionsAsked]).getAnswer();
			questions.get(questionNumbers[questionsAsked]).setAnswer(-1);
	  	  	return questions.get(questionNumbers[questionsAsked]);
		}
  	 }
	
	@RequestMapping(value = "/getResult", method = RequestMethod.GET)
	public @ResponseBody String getResult(@RequestParam String res) {
		int answer = Integer.parseInt(res)+1;
		if(currentCorrectAnswer==answer)
			correctAnswers++;
		
		Result result = new Result(); //�������� ���������� � ��
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		result.setUsername(user.getUsername());
		result.setResult(correctAnswers);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		result.setDate(dateFormat.format( new Date() ));
		resultService.addResult(result);
		return ""+correctAnswers;
	}
	
	@RequestMapping("/stats")
	public String stats(Model model) {
		List<Result> results= resultService.listResult();
		model.addAttribute("results", results);
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("myName", user.getUsername());
		return "stats";
	}
	
	private boolean contains(int[] array, int num) {
		for(int i=0;i<array.length;i++)
			if(array[i]==num)
				return true;
		return false;
	}
}
