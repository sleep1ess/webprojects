package ru.kulakov.testsystem.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.kulakov.testsystem.domain.User;
import ru.kulakov.testsystem.service.UserService;

@Controller
@RequestMapping("/")
public class UserController {
	
	@Autowired
    private UserService userService;
	
	@RequestMapping("/")
	public String index(Model model) {
		return "redirect:login";
	}
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("user", new User());
		return "login";
	}
	
	 @RequestMapping(value = "/addUser", method = RequestMethod.POST)
	    public String add(@ModelAttribute("user") User user,
	            BindingResult result) {

		 	user.setAuthority("ROLE_USER");
	        userService.addUser(user);

	        return "redirect:/login";
	    }

}
